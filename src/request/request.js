import axios from "axios";
import { config } from "vue/types/umd";

const instance = axios.create({
    baseURL: "http://kumanxuan1.f3322.net:8881/cms",
    timeout: 5000
})

// 请求拦截器
instance.interceptors.request.use(config => {
    // 添加请求头
    let token = localStorage.getItem("x-auth-token")
    if (token) {
        // 判断是否存在token，如果存在的话，则每个http header都加上token
        config.headers["x-auth-token"] = token; //请求头加上token
    }
    return config;
}, err => {
    return Promise.reject(err)
})

// 响应拦截器
instance.interceptors.response.use(config => {
    return config;
}, err => {
    return Promise.reject(err)
})

export default instance