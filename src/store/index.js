import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // 登录框状态
    isOpenLogin: false,
    toast: {
      show: false,
      icon: "icon-toast_chenggong"
    }
  },
  getters: {
  },
  mutations: {
    // 修改登录框状态
    changeLoginShow(state, payload) {
      state.isOpenLogin = payload
    },
    // 打开toast
    openToast(state, payload) {
      state.toast.show = true,
        state.toast.content = payload.content
      switch (payload.icon) {
        case "info":
          // 警告
          state.toast.icon = "icon-toast-jinggao";
          break;
        case "success":
          // 成功
          state.toast.icon = "icon-toast_chenggong";
          break;
        case "danger":
          // 失败
          state.toast.icon = "icon-toast-shibai_huaban";
          break;
        case "loading":
          // 加载中
          state.toast.icon = "icon-loading";
          break;
        default:
          state.toast.icon = "icon-toast-jinggao";
          break;
      }
    },
    // 同步关闭警示框
    closeToast(state) {
      state.toast.show = false
    }
  },
  actions: {
  },
  modules: {
  }
})
